'use strict';

const { parse } = require('querystring');

module.exports.btssearch = async event => {

  let postData = {};
  try {
    postData = parse(event.body);
  } catch (err) { }
 
  console.log('BtsSearch input data: ' + JSON.stringify(postData));

  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Thanks for BtsSearch new data :)',
        received: postData
      },
      null,
      2
    ),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
